FROM nginx:stable-alpine
COPY default.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY /build/ .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
