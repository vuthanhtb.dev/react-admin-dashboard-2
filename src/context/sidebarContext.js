import React, { useState, createContext, useContext } from 'react';
import { ProSidebarProvider } from 'react-pro-sidebar';

const SidebarContext = createContext({});

export const SidebarProvider = ({ children }) => {
  const [sidebarBackgroundColor, setSidebarBackgroundColor] = useState(undefined);
  const [sidebarImage, setSidebarImage] = useState(undefined);

  return (
    <ProSidebarProvider>
      <SidebarContext.Provider
        value={{
          sidebarBackgroundColor,
          setSidebarBackgroundColor,
          sidebarImage,
          setSidebarImage,
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          {children}
        </div>
      </SidebarContext.Provider>
    </ProSidebarProvider>
  );
};

export const useSidebarContext = () => useContext(SidebarContext);
