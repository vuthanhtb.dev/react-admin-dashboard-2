import { Route, Routes } from 'react-router-dom';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { ColorModeContext, useMode } from 'theme';
import Sidebar from 'components/Sidebar';
import TopBar from 'components/TopBar';
import Dashboard from 'pages/dashboard';
import Team from 'pages/team';
import Contacts from 'pages/contacts';
import Invoices from 'pages/invoices';
import ProfileForm from 'pages/profileForm';
import Bar from 'pages/bar';
import Pie from 'pages/pie';
import Line from 'pages/line';
import FAQ from 'pages/faq';
import Calendar from 'pages/calendar';
import Geography from 'pages/geography';
import { SidebarProvider } from 'context/sidebarContext';

function App() {
  const [theme, colorMode] = useMode();

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <SidebarProvider>
          <div className="app">
            <Sidebar />
            <main className="content">
              <TopBar />
              <Routes>
                <Route path="/" element={<Dashboard />} />
                <Route path="/team" element={<Team />} />
                <Route path="/contacts" element={<Contacts />} />
                <Route path="/invoices" element={<Invoices />} />
                <Route path="/form" element={<ProfileForm />} />
                <Route path="/bar" element={<Bar />} />
                <Route path="/pie" element={<Pie />} />
                <Route path="/line" element={<Line />} />
                <Route path="/faq" element={<FAQ />} />
                <Route path="/calendar" element={<Calendar />} />
                <Route path="/geography" element={<Geography />} />
              </Routes>
            </main>
          </div>
        </SidebarProvider>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;
